package sample

import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll

object ShrinkList extends Properties("List") {

  import org.scalacheck.Arbitrary._
  val gl : Gen[List[Int]] = arbitrary[List[Int]]

  property("test list") = forAll(gl)( l => l.size < 20 )

}
