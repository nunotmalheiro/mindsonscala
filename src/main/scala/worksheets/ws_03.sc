// Pattern MAtching

val tuple1 = (1,"one", List(1,2,3))
val (number,literal, listHead :: listTail) = tuple1

def tupleToInt(t: (Int, String, List[Int])): Int = t match {
  case (number,literal, listHead :: listTail) => number
  case (number,literal, Nil) => number * 2
}
tupleToInt((1,"one", List()))

// In Partial Functions

val l: List[(Int,String,List[Int])] =
  List(
    (1,"one",List(1)),
    (2,"two",List(1,2)),
    (3,"three",List(1,2,3))
  )

l.map{ case (n,e,_) => s"$n -> $e"}