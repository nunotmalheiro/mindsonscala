// Simple Value

val a = 10
val b = a + 1
val c : String = "Word"

// Lazy Values

lazy val la = 10
lazy val lb = a + 1
lazy val lc : String = "Word"

// Function definitions

def square(x: Double): Double = a + b
square(4)


// Simple Function

def add(a: Int, b: Int): Int = a + b
add(1,2)

// Curried Function

def addc(a: Int)(b: Int): Int = a+b
addc(1)(2)

// Generic Curried Function

def toTuple[A,B](a: A)(b: B): (A,B) = (a,b)
toTuple(1)("one")

// Higher Order Generic Curried Function

def operate[A](a: A, b: A)(f: (A,A)=> A): A =
  f(a , b)

operate(1,2)((a,b) => a+b)
operate(1,2)(_ + _)
operate(1,2)(Integer.sum)

// Values as Functions

val fsquare: Int => Double =   a => a*a

val fcube: Int => Double =   a => a*a*a

val fexp: Int => Double =   a => math.exp(a)

fsquare(2)

fcube(3)

def operate(f: Int => Double)(v: Int): Double = f(v)

operate(fsquare)(2)

operate(fcube)(3)