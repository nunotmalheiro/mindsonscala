import java.io.{BufferedReader, File, FileReader}

import scala.io.Source
import scala.util.Try

def linesOnFile(fn: String): Int = {
  val fr = new FileReader(fn)
  val br = new BufferedReader(fr)

  var line = br.readLine()

  var count = 0

  while (line != null) {
    val words = line.split("\\s")

    count += words.size

    line = br.readLine()
  }

  br.close()
  count
}

// use an existing file name
def linesOnFileFun(fn: String): Int = {
  Source.fromFile(fn).getLines().
    map(l => l.split("\\s").size).sum
}

// use an existing file name
def linesOnFileF2(fn: String): Option[Int] = {
  Try(Source.fromFile(fn).getLines()).toOption.
    map(is => is.map(l => l.split("\\s").size).sum )
}


// This is the current folder
val current = new File(".").getCanonicalPath
System.out.println("Current dir:" + current)

// use an existing file name
val fn = ""


linesOnFile(fn)

linesOnFileFun(fn)

linesOnFileF2(fn)