import scala.annotation.tailrec


// Imperative Loop

val l = List(1,5,10,25)

def sum(l: List[Int]): Int = {
  var i = 0
  var t = 0
  while (i<l.size) {
    t += l(i)
    i+=1
  }
  t
}

sum(l)

// Functional Loop with Possible stack overflow !
def functionalSum(l: List[Int]): Int = l match {
  case h :: t => h + functionalSum(t)
  case Nil => 0
}

functionalSum(l)

// Functional Tail Recursive Loop
def functionalSumTailRec(l: List[Int]): Int = {
  @tailrec
  def loop(l: List[Int], s: Int): Int = l match {
    case h :: t => loop(t, s+h)
    case Nil => s
  }
  loop(l,0)
}

functionalSumTailRec(l)
