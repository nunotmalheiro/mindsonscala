// For comprehension

val l1 = List.range(1,5)
val l2 = List(2,4,6,8,10)

// We would like to generate
// List((1,2), (1,4), (1,6), (1,8), (1,10), (2,2), (2,4), (2,6), (2,8) ...

l1.map(e1 =>
  l2.map(e2 =>
    (e1,e2)
  ))

l1.flatMap(e1 =>
  l2.map(e2 =>
    (e1,e2)
  ))

for (
  e1 <- l1 ;
  e2 <- l2
) yield (e1,e2)

// If guards to remove equal elements
for (
  e1 <- l1 ;
  e2 <- l2
  if (e1!=e2)
) yield (e1,e2)
