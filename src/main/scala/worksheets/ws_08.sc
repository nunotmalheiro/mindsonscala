// Implicit parameters

def myFunc(a: Int)(implicit s: String): String =
  a.toString +" "+ s

myFunc(100)("Miles")

implicit val implicitS: String = "Implicit Miles!"

myFunc(100)("Miles")

myFunc(100)

def myFunci(a: Int): String =
  a.toString +" "+ implicitly[String]

//implicit val implicitS: String = "Implicit Miles!"

myFunci(100)

// Implicit conversions

trait Show[A] {
  def show: String
}

implicit def intToShow(x: Int): Show[Int] =
  new Show[Int] {
    override def show: String = s"int $x"
  }

val i: Int = 12

i.show
