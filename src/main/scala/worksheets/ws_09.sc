// Error Handling with exceptions:

def average(s: Seq[Double]): Double =
  if (s.isEmpty)
    throw new ArithmeticException("mean of empty list!")
  else s.sum / s.length


//Error Handling with types:

def averageO(s: Seq[Double]): Option[Double] =
  if (s.isEmpty)
    None
  else Some(s.sum / s.length)

// Error handling with getOrElese

trait Employee
case object Marketing extends Employee
case object Engineering extends Employee
case object Management extends Employee

def employeeDepartment(c: Employee): Option[String] = c match {
  case Marketing => Some("Sales")
  case Engineering=> Some("Development")
  case Management => None
}

employeeDepartment(Engineering).map(_.toLowerCase).getOrElse("default")

employeeDepartment(Management).map(_.toLowerCase).getOrElse("default")

// Advantages of switching the conversion to lower case?
employeeDepartment(Management).getOrElse("default").toLowerCase
