import scala.annotation.tailrec

@tailrec
def findNum(n:Int, l: List[Int],le: List[Int], c: Int): Int =
  if (le.isEmpty) c else {
    val nle = le.flatMap(e => l.map(i => e+i) ).filter(_ <= n)
    val (less,equal) = nle.partition(_  < n)
    println(nle)
    println(less)
    println(equal)
    println("***")
    findNum(n, l, less, c+equal.size)
  }

def numSteps(n:Int, l: List[Int]): Int =
  findNum(n, l, List(0), 0)

numSteps(3,List(1,2))