package tap

import org.scalatest.FunSuite

class AgendaTest extends FunSuite {

  test("testSchedule normal") {

    val n1 = Nurse("1",Set(RoleA,RoleB)) ; val n2 = Nurse("2",Set(RoleB,RoleC)) ; val n3 = Nurse("3",Set(RoleA,RoleC))
    val ln = List( n1 , n2 , n3 )

    val r1 = Requirement(RoleB,1) ; val r2 = Requirement(RoleA,2) ; val r3 = Requirement(RoleC,2)
    val p1 = Period("manha",List(r1)) ; val p2 = Period("tarde",List(r2)) ; val p3 = Period("noite",List(r3))
    // Normal schedule
    val lp = List( p1, p2, p3)

    val rlp = Some(List(
      Shift(n1,p1,r1),
      Shift(n1,p2,r2), Shift(n3,p2,r2),
      Shift(n2,p3,r3), Shift(n3,p3,r3)
    ))

    assert(Agenda(ln,lp).schedule==rlp)
  }

  test("testSchedule non existent skill") {

    val n1 = Nurse("1",Set(RoleA,RoleB)) ; val n2 = Nurse("2",Set(RoleB,RoleC)) ; val n3 = Nurse("3",Set(RoleA,RoleC))
    val ln = List( n1 , n2 , n3 )

    val p1 = Period("manha",List(Requirement(RoleB,1))) ; val p2 = Period("tarde",List(Requirement(RoleA,2)))

    // RoleD does not exist in the nurses skils'
    val lp = List(p1,p2,Period("noite",List(Requirement(RoleD,1))))

    assert(Agenda(ln, lp).schedule.isEmpty)
  }

  test("testSchedule required more nurses than exist") {

    val n1 = Nurse("1",Set(RoleA,RoleB)) ; val n2 = Nurse("2",Set(RoleB,RoleC)) ; val n3 = Nurse("3",Set(RoleA,RoleC))
    val ln = List( n1 , n2 , n3 )

    val p1 = Period("manha",List(Requirement(RoleB,1))) ; val p2 = Period("tarde",List(Requirement(RoleA,2)))

    // There are less than 3 nurses with role A
    val lp = List(p1,p2,Period("noite",List(Requirement(RoleA,3))))

    assert(Agenda(ln, lp).schedule.isEmpty)
  }

  test("testSchedule Multiple Requirements") {
    val n1 = Nurse("1",Set(RoleA,RoleB)) ; val n2 = Nurse("2",Set(RoleB,RoleC)) ; val n3 = Nurse("3",Set(RoleA,RoleC))
    val ln = List( n1, n2 , n3 )

    val r1 = Requirement(RoleB,2) ; val r2 = Requirement(RoleA,1)

    val p = Period("manha",List(r1,r2))
    val lp = List( p )

    val ls = Some(List(
      Shift(n1, p, r1),
      Shift(n2, p, r1),
      Shift(n3, p, r2),

    ))
    assert(Agenda(ln,lp).schedule === ls)
  }

}