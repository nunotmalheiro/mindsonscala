import org.scalacheck.Gen

val myGen = for {
  n <- Gen.choose(10,20)
  m <- Gen.choose(2*n, 500)
} yield (n,m)


val vowel = Gen.oneOf('A', 'E', 'I', 'O', 'U', 'Y')

val genIntList = Gen.containerOf[List,Int](Gen.oneOf(1, 3, 5))

val genStringList = Gen.containerOf[List,String](Gen.alphaStr)

val genBoolArray  = Gen.containerOf[Array,Boolean](true)

def matrix[T](g: Gen[T]): Gen[Seq[Seq[T]]] = Gen.sized { size =>
  val side = scala.math.sqrt(size).asInstanceOf[Int]
  Gen.listOfN(side, Gen.listOfN(side, g))
}

genIntList.sample

sealed abstract class Tree
case class Node(left: Tree, right: Tree, v: Int) extends Tree
case object Leaf extends Tree

val genLeaf = Gen.const(Leaf)

val genNode = for {
  v <- Gen.chooseNum(0,1000)
  left <- genTree
  right <- genTree
} yield Node(left, right, v)

def genTree: Gen[Tree] = Gen.oneOf(genLeaf, genNode)

genNode.sample
genTree.sample