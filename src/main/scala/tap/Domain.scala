package tap

sealed trait Role
case object RoleA extends Role
case object RoleB extends Role
case object RoleC extends Role
case object RoleD extends Role

case class Nurse(id: String, skills: Set[Role])

case class Requirement(r: Role, quantity: Int)

case class Period(id: String, lr: List[Requirement])

case class Shift(n: Nurse, p:Period, r: Requirement)

case class Agenda(ln: List[Nurse], lp: List[Period]) {

  def scheduleOneRequirement(ln: List[Nurse],p: Period, r: Requirement): List[(List[Shift],List[Nurse])] = {
    val lnp = ln.filter(_.skills.contains(r.r))
    lnp.combinations(r.quantity).map(lnc => (lnc.map( n => Shift(n,p,r)), ln.diff(lnc))).toList
  }

  def scheduleOnePeriod(ln: List[Nurse], p: Period): List[List[Shift]] = {
    p.lr.foldLeft[List[(List[Shift],List[Nurse])]](List((List(),ln))) {
      case (lls,r) =>
        lls.flatMap{ case (ls,ln) => scheduleOneRequirement(ln,p,r).map{
          case (lsp,lnp) => (ls ::: lsp,lnp) } }
    }.map(_._1)
  }

  def schedule: Option[List[Shift]] =
    lp.foldLeft[List[List[Shift]]](List(List())) { case (lls,p) =>
      lls.flatMap(ls => scheduleOnePeriod(ln, p).map(lsp => ls ::: lsp))
    }.headOption

}