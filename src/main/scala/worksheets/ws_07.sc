class Entity(val id: String, val properties: List[Int])

val entity1 = new Entity("This", List(1,2,3))
val entity2 = new Entity("This", List(1,2,3))

entity1 == entity2  // false


case class CaseEntity(id: String, properties: List[Int])

val caseEntity1 =  CaseEntity("This", List(1,2,3))
val caseEntity2 =  CaseEntity("This", List(1,2,3))

caseEntity1==caseEntity2  // true

sealed trait Color
case object Red extends Color
case object Green extends Color
case object Blue extends Color

sealed trait Toy
case object Ball extends Toy
case object Blanket extends Toy


trait Animal
case class Dog(c: Color) extends Animal
case class Cat(c: Color,toy: Toy) extends Animal

def colorDye(c: Color): Color = c match {
  case Red => Green
  case Green => Blue
    // Warning
}

val cat = Cat(Red,Blanket)
val diedCat = cat.copy(c = colorDye(cat.c))