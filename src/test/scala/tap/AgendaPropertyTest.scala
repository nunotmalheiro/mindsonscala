package tap

import org.scalacheck.Gen
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object AgendaPropertyTest extends Properties("Agenda Properties") {

  // ------------------  Generate nurse list --------------------
    // Generate one nurse
    val gNurse: Gen[Nurse] = for (
      name <- Gen.listOfN(10, Gen.alphaLowerChar);
      roles <- Gen.nonEmptyContainerOf[Set, Role](Gen.oneOf(RoleA, RoleB, RoleC, RoleD))
    ) yield Nurse(name.mkString, roles)

    // Generate a maximum of 7 nurses
    val glNurses: Gen[List[Nurse]] = Gen.sized {
      s => Gen.listOfN(math.max(1,math.min(7, s)), gNurse)
    }
  // ---------------------------------------------------------------

  // ------------------  Generate requirements  --------------------
  // Generate requirements from nurses
  def requirementsFromNurses(ln: List[Nurse]): List[Requirement] =
    ln.map(n => Requirement(n.skills.head,1)).groupBy(_.r).map{ case (r,lr) => Requirement(r,lr.size)}.toList

  // ---------------------  Properties -----------------------------
  property("test Valid Schedule") =
    forAll(glNurses)(ln => Agenda(ln,List(Period("manha",requirementsFromNurses(ln)))).schedule.isDefined )

  property("test Invalid Schedule") =
    forAll(glNurses)(ln => {
      val lr = requirementsFromNurses(ln)
      Agenda(ln, List(Period("manha", lr.head :: lr))).schedule.isEmpty
    })

}