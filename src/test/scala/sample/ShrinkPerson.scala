package sample

import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Properties}

case class Person(name: String, age: Int) {
  def birthday: Person = copy(age = age + 1)
}

object ShrinkPerson extends Properties("List") {

  import org.scalacheck.Arbitrary._
  val gPerson : Gen[Person] = for (
    n <- arbitrary[String] ;
    a <- arbitrary[Int] // Replace with gen to make the property valid
  ) yield Person(n,a)

  property("birthdays make you older") = forAll(gPerson)( p =>
    p.birthday.age > p.age )

}
