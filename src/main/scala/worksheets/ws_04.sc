// Generic Algorithms

val l1 = List.range(1,10)
val l2 = List(2,4,6,8,10)

l2.forall(i => (i % 2 == 0))

def isPrime(i:Int):Boolean =
  (2 to math.sqrt(i).toInt).forall(x => i % x != 0)

l1.exists(i => isPrime(i) )

l1.map( i => i*2 )

l1.filter(i => (i % 2 == 0))

l1.zip(l2)

l2.foldLeft(0) { case (a,i) => a+i }  // Int = 30