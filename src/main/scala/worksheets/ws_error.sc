def average(s: Seq[Double]): Option[Double] =
  if (s.isEmpty)
    None
  else Some(s.sum / s.length)

val s: Seq[Seq[Double]] = Seq(Seq(1,2,3),Seq(3,4,5),Seq())

// Ignore
def averageOfAverages(ss: Seq[Seq[Double]]): Option[Double] =
  average( ss.map(s => average(s)).flatten )

averageOfAverages(s)

def averageOfAveragesBubble(ss: Seq[Seq[Double]]): Option[Double] = {
  val sod : Seq[Option[Double]] = ss.map(sd => average(sd))
  val osd = sod.foldLeft[Option[Seq[Double]]](Some(Seq())) {
    case (osd, od) => osd.flatMap(sd => od.map( d => sd :+ d))
  }
  osd.flatMap(average(_))
}
averageOfAveragesBubble(s)


// Cats example does not function well in a worksheet. Use console!
/*
import cats.implicits._
def averageOfAveragesCats(ss: Seq[Seq[Double]]): Option[Double] = {
  ss.toList.traverse(average).flatMap(average(_))
}
averageOfAveragesCats(s)

*/